package com.korbiak.model;


import com.korbiak.model.object.Device;
import com.korbiak.model.object.Devices;
import com.korbiak.model.parse.SAXHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;


import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Model {
    private Devices devices;


    public String getSAX() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler handler = new SAXHandler();
            saxParser.parse(new File("src/main/resources/xml/devices.xml"), handler);
            devices = handler.getDeviceList();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return "Parsing completed";
    }

    public String getDOM() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("E:\\EPUM\\task14_xml\\src\\main\\resources\\xml\\devices.xml"));
        document.getDocumentElement().normalize();
        NodeList nList = document.getElementsByTagName("device");
        devices = new Devices();
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                Device device = new Device();
                device.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                device.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                device.setPrice(Integer.parseInt(eElement.getElementsByTagName("price").item(0).getTextContent()));
                device.setPeripheral(Boolean.valueOf(eElement.getElementsByTagName("peripheral").item(0).getTextContent()));
                device.setEnergyConsumption(Integer.parseInt(eElement.getElementsByTagName("energyConsumption").item(0).getTextContent()));
                device.setCooler(Boolean.valueOf(eElement.getElementsByTagName("cooler").item(0).getTextContent()));
                device.setGroup(eElement.getElementsByTagName("group").item(0).getTextContent());
                device.setCOM(Boolean.parseBoolean(eElement.getElementsByTagName("COM").item(0).getTextContent()));
                device.setUSB(Boolean.parseBoolean(eElement.getElementsByTagName("USB").item(0).getTextContent()));
                device.setLPT(Boolean.parseBoolean(eElement.getElementsByTagName("LPT").item(0).getTextContent()));
                devices.deviceList.add(device);
            }
        }
        return "Parsing completed";
    }

    public String printList() {
        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < devices.deviceList.size(); i++) {
            answer.append(devices.deviceList.get(i)).append("\n");
        }
        if (devices.deviceList.isEmpty()) return "List empty";
        return String.valueOf(answer);
    }

    public void sort() {
        devices.sort();
    }

}
