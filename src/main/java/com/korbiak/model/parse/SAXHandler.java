package com.korbiak.model.parse;

import com.korbiak.model.object.Device;
import com.korbiak.model.object.Devices;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {

    private static final String DEVICES = "devices";
    private static final String DEVICE = "device";
    private static final String NAME = "name";
    private static final String ORIGIN = "origin";
    private static final String PRICE = "price";
    private static final String TYPE = "type";
    private static final String PERIPHERAL = "peripheral";
    private static final String ENERGY = "energyConsumption";
    private static final String COOLER = "cooler";
    private static final String GROUP = "group";
    private static final String PORTS = "ports";
    private static final String USB = "USB";
    private static final String COM = "COM";
    private static final String LPT = "LPT";

    private Devices devices;
    private String elementValue;


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        elementValue = new String(ch, start, length);
    }

    @Override
    public void startDocument() throws SAXException {
        devices = new Devices();
    }

    @Override
    public void startElement(String uri, String lName, String qName, Attributes attr) throws SAXException {
        switch (qName) {
            case DEVICES:
                devices.setDeviceList(new ArrayList<>());
                break;
            case DEVICE:
                devices.deviceList.add(new Device());
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case NAME:
                latestDevice().setName(elementValue);
                break;
            case ORIGIN:
                latestDevice().setOrigin(elementValue);
                break;
            case PRICE:
                latestDevice().setPrice(Integer.parseInt(elementValue));
                break;
            case PERIPHERAL:
                latestDevice().setPeripheral(Boolean.valueOf(elementValue));
                break;
            case ENERGY:
                latestDevice().setEnergyConsumption(Integer.parseInt(elementValue));
                break;
            case COOLER:
                latestDevice().setCooler(Boolean.valueOf(elementValue));
                break;
            case GROUP:
                latestDevice().setGroup(elementValue);
                break;
            case USB:
                latestDevice().setUSB(Boolean.parseBoolean(elementValue));
                break;
            case COM:
                latestDevice().setCOM(Boolean.parseBoolean(elementValue));
                break;
            case LPT:
                latestDevice().setLPT(Boolean.parseBoolean(elementValue));
                break;
        }
    }

    private Device latestDevice() {
        List<Device> deviceList = devices.getDeviceList();
        int latestIndex = deviceList.size() - 1;
        return deviceList.get(latestIndex);
    }

    public Devices getDeviceList() {
        return devices;
    }

}
