package com.korbiak.model.object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Devices {

    public List<Device> deviceList;

    public Devices() {
        this.deviceList = new ArrayList<>();
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }

    public void sort(){
        Collections.sort(deviceList);
    }
}
