package com.korbiak.model.object;

public class Device implements Comparable<Device> {
    private String name;
    private String origin;
    private int price;
    private Type type;

    public Device() {
        type = new Type();
    }

    public Device(String name, String origin, int price, Boolean peripheral, int energyConsumption,
                  Boolean cooler, String group, boolean COM, boolean USB, boolean LPT) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = new Type(peripheral, energyConsumption, cooler, group, COM, USB, LPT);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setUSB(boolean USB) {
        type.setUSB(USB);
    }

    public void setLPT(boolean LPT) {
        type.setLPT(LPT);
    }

    public void setCOM(boolean COM) {
        type.setCOM(COM);
    }

    public boolean getCOM() {
        return type.getCOM();
    }

    public boolean getUSB() {
        return type.getUSB();
    }

    public boolean getLPT() {
        return type.getLPT();
    }

    public Boolean getPeripheral() {
        return type.getPeripheral();
    }

    public void setPeripheral(Boolean peripheral) {
        type.setPeripheral(peripheral);
    }

    public int getEnergyConsumption() {
        return type.getEnergyConsumption();
    }

    public void setEnergyConsumption(int energyConsumption) {
        type.setEnergyConsumption(energyConsumption);
    }

    public Boolean getCooler() {
        return type.getCooler();
    }

    public void setCooler(Boolean cooler) {
        type.setCooler(cooler);
    }

    public String getGroup() {
        return type.getGroup();
    }

    public void setGroup(String group) {
        type.setGroup(group);
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", type=" + type +
                '}';
    }

    @Override
    public int compareTo(Device o) {
        return Integer.compare(this.price, o.price);
    }
}
