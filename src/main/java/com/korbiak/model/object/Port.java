package com.korbiak.model.object;

public class Port {
    private boolean COM;
    private boolean USB;
    private boolean LPT;

    public Port() {
    }

    public Port(boolean COM, boolean USB, boolean LPT) {
        this.COM = COM;
        this.USB = USB;
        this.LPT = LPT;
    }

    public boolean isCOM() {
        return COM;
    }

    public void setCOM(boolean COM) {
        this.COM = COM;
    }

    public boolean isUSB() {
        return USB;
    }

    public void setUSB(boolean USB) {
        this.USB = USB;
    }

    public boolean isLPT() {
        return LPT;
    }

    public void setLPT(boolean LPT) {
        this.LPT = LPT;
    }

    @Override
    public String toString() {
        return "Port{" +
                "COM=" + COM +
                ", USB=" + USB +
                ", LPT=" + LPT +
                '}';
    }
}
