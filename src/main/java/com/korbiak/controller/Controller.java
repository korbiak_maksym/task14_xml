package com.korbiak.controller;

import com.korbiak.model.Model;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Controller {

    public Model model;

    public Controller() {
        this.model = new Model();
    }

    public String getSAX() {
        return model.getSAX();
    }

    public String getDOM() {
        try {
            return model.getDOM();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return "err";
    }

    public String print() {
        return model.printList();
    }

    public void sort() {
       model.sort();
    }
}
